from django.shortcuts import render, redirect
from .models import MyModel
from .forms import New


# Create your views here.
def index(request):
    mce = MyModel.objects.all()
    return render(request, 'mce/index.html', {'mce': mce})


def new(request):
    if request.method == "POST":
        form = New(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('index', )
    else:
        form = New()
    return render(request, 'mce/new.html', {'form': form})
