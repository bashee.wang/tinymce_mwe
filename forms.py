from django import forms
from .models import MyModel


class New(forms.ModelForm):

    class Meta:
        model = MyModel
        fields = ('my_field', )
