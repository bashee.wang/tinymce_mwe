from django.db import models
from tinymce import HTMLField


# Create your models here.
class MyModel(models.Model):
    my_field = HTMLField('Text')
